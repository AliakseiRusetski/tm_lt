from django.core.management import call_command
from django.core.management.base import AppCommand
from django.db import connections


class Command(AppCommand):
    help = 'Truncate app datatables'

    def handle_app_config(self, app_config, **options):
        self.stdout.write("trunct_tables initialized")

        with connections["default"].cursor() as c:
            tm_table_names = (
                "ticketmaster_adapter_promoting",
                "ticketmaster_adapter_promoter",
                "ticketmaster_adapter_pricerange",
                "ticketmaster_adapter_sale",
                "ticketmaster_adapter_event",
            )
            for tm_table in tm_table_names:
                c.execute("""
                DELETE FROM {} WHERE 1;
                """.format(tm_table)).fetchone()
                self.stdout.write("DELETED {}".format(tm_table))
            # c.execute("""
            #     DELETE FROM django_migrations WHERE app = ?;
            # """, (app_config.name,))

        # call_command("migrate")
        self.stdout.write("trunct_tables completed")

        pass
