from django.apps import AppConfig


class TicketmasterAdapterConfig(AppConfig):
    name = 'ticketmaster_adapter'
