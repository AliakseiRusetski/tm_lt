from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.test import APITestCase

from . import mocks


class SomeTest(APITestCase):
    fixtures = ["ticketmaster_adapter/fixtures/test_dataset.json"]

    def setUp(self) -> None:
        self.client = APIClient()
        # call_command('loaddata', 'fixtures/myfixture', verbosity=0)
        self.super_user = User.objects.create_superuser(username="su_login", password="su_password")

    def test_events_inaccessible(self):
        """
        Ensure unauthenticated user doesn't have access to raw post
        """
        url = reverse('raw_event_poster')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        pass

    def test_events_accessible(self):
        """
        Ensure authenticated user can post raw events
        """
        self.client.login(username='su_login', password='su_password')
        url = reverse('raw_event_poster')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.logout()

    def test_events_count(self):
        """
        Check events count works on GET
        """
        url = reverse('event-list')
        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data["count"], 2)

    def test_promoters_count(self):
        """
        Check promoters count works on POST
        """
        url = reverse('promoter-list')
        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data["count"], 2)

    def test_event_put(self):
        """
        Check event changed on PUT
        """
        self.maxDiff = None
        url = reverse('event-detail', kwargs={'pk': 4386})
        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        put_with_selves = {**response.json()}
        response = self.client.put(url, mocks.event_4386_put)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        for k, v in mocks.event_4386_put.items():
            put_with_selves[k] = v
        self.assertDictEqual(response.json(), put_with_selves)

        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.json(), put_with_selves)

    def test_event_patch(self):
        """
        Check event changed on PATCH
        """
        self.maxDiff = None
        url = reverse('event-detail', kwargs={'pk': 4386})
        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        response = self.client.patch(url, mocks.event_4386_patch)
        patched_with_selves = {**response.json()}
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        for k, v in mocks.event_4386_patch.items():
            patched_with_selves[k] = v
        self.assertDictEqual(response.json(), patched_with_selves)

        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.json(), patched_with_selves)

    def test_search(self):
        """
        Check searching by start_date and promoter fields
        """
        url = reverse('event-list')
        response = self.client.get(url, data={
            "start_date": "2020-04-04",
            "promoter": 1
        })
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data["count"], 2)
