event_4386_put = {
    "id": 4386,
    "name": "2",
    "type": "event",
    "url": "http://i forgot url validation",
    "locale": "en-us",
    "info": "2",
    "pleaseNote": "2",
    "dates_timezone": "America/Indianapolis",
    "dates_spanMultipleDays": True,
    "dates_start_dateTime": "2020-04-04T16:54:12Z",
    "dates_start_dateTBD": False,
    "dates_start_dateTBA": True,
    "dates_start_timeTBA": False,
    "dates_start_noSpecificTime": True,
    "dates_status_code": "rescheduled",
    "sales": [
        {
            "startDateTime": "2020-04-04T16:55:03Z",
            "endDateTime": "2020-04-04T16:55:06Z",
            "startTBD": True,
            "type": "public",
            "name": "3",
            "description": "3",
            "url": "3"
        }
    ],
    "priceRanges": [
        {
            "type": "standard",
            "currency": "USD",
            "min": 10.0,
            "max": 12.0
        }
    ]
}

event_4386_patch = {
    "info": "More verbose Info",
    "pleaseNote": "More verbose Note",
    "priceRanges": [
        {
            "type": "standard",
            "currency": "USD",
            "min": 5.0,
            "max": 12.0
        }
    ]
}
