from django.contrib import admin

from . import models

admin.site.register(models.Promoting)
admin.site.register(models.Event)
admin.site.register(models.PriceRange)
admin.site.register(models.Sale)
admin.site.register(models.Promoter)
