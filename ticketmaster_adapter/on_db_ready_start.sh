#!/bin/sh

echo "Checking for db ${SQL_HOST} ${SQL_PORT}"
while ! nc -z $SQL_HOST $SQL_PORT; do
    sleep 0.1
    echo "Waiting for db ${SQL_HOST} ${SQL_PORT}"
done
echo "DB Ready"

echo "Migration"
python manage.py migrate
exec "$@" # run CMD as primary process
