from django.db import models


class Event(models.Model):  # model -> sql
    name = models.CharField(max_length=255)
    TYPE_CHOICES = ((0, 'event'),)
    type = models.SmallIntegerField(choices=TYPE_CHOICES)
    url = models.CharField(max_length=255, )
    LOCALE_CHOICES = ((0, 'en-us'),)
    locale = models.SmallIntegerField(choices=LOCALE_CHOICES)
    info = models.TextField(null=True)
    pleaseNote = models.TextField(null=True)
    DATES_TIMEZONE_CHOICES = (
        (0, 'America/Chicago'), (1, 'Europe/Amsterdam'), (2, 'Europe/London'), (3, 'Europe/Madrid'),
        (4, 'America/Phoenix'),
        (5, 'America/New_York'), (6, 'America/Indianapolis'), (7, 'America/Denver'), (8, 'Europe/Brussels'),
        (9, 'America/Los_Angeles'))
    dates_timezone = models.SmallIntegerField(null=True, choices=DATES_TIMEZONE_CHOICES)
    dates_spanMultipleDays = models.BooleanField()
    dates_start_dateTime = models.DateTimeField(null=True)
    dates_start_dateTBD = models.BooleanField()
    dates_start_dateTBA = models.BooleanField()
    dates_start_timeTBA = models.BooleanField()
    dates_start_noSpecificTime = models.BooleanField()
    DATES_STATUS_CODE_CHOICES = ((0, 'offsale'), (1, 'rescheduled'), (2, 'cancelled'), (3, 'onsale'))
    dates_status_code = models.SmallIntegerField(choices=DATES_STATUS_CODE_CHOICES)


class Sale(models.Model):
    startDateTime = models.DateTimeField(null=True, blank=True)
    startTBD = models.BooleanField(null=True, blank=True)
    endDateTime = models.DateTimeField(null=True, blank=True)
    TYPE_CHOICES = ((0, 'public'), (1, 'presales'))
    type = models.SmallIntegerField(choices=TYPE_CHOICES)
    name = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    url = models.CharField(max_length=255, null=True, blank=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name="sales")


class PriceRange(models.Model):
    TYPE_CHOICES = ((0, 'standard'), (1, 'standard including fees'))
    type = models.SmallIntegerField(choices=TYPE_CHOICES)
    CURRENCY_CHOICES = ((0, 'USD'), (1, 'CAD'), (2, 'GBP'))
    currency = models.SmallIntegerField(choices=CURRENCY_CHOICES)
    min = models.FloatField()
    max = models.FloatField()
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name="priceRanges")


class Promoter(models.Model):
    name = models.CharField(max_length=255, )
    description = models.CharField(max_length=255, null=True)
    event = models.ManyToManyField(Event, through="Promoting", related_name="promoters")


class Promoting(models.Model):
    promoter = models.ForeignKey(Promoter, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    main = models.BooleanField()

    class Meta:
        unique_together = (("promoter", "event"),)
