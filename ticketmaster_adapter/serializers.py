import django_filters
from rest_framework import serializers

from ticketmaster_adapter import models


class BidirectionalChoiceField(serializers.ChoiceField):
    """
    Fixed representation bug at ChoiceField and created sub Field.
    """
    # Property choice omitted
    def __init__(self, choices, **kwargs):
        super().__init__(choices, **kwargs)
        self.choice_k_to_v = {
            str(k): v for k, v in self._choices.items()
        }
        self.choice_v_to_k = {
            v: str(k) for k, v in self._choices.items()
        }

    def to_representation(self, value):
        if value in ('', None):
            return value
        return self.choice_k_to_v.get(str(value), value)

    def to_internal_value(self, data):
        if data == '' and self.allow_blank:
            return ''

        try:
            return self.choice_v_to_k[str(data)]
        except KeyError:
            self.fail('invalid_choice', input=data)


class EventFilter(django_filters.FilterSet):
    """
    Allows search by name, start_date, cost range, and promoter name
    """
    name = django_filters.CharFilter(field_name="name", lookup_expr="icontains")
    start_date = django_filters.DateTimeFilter(field_name="dates_start_dateTime", lookup_expr="gte")
    promoter = django_filters.CharFilter(label="Supporting promoter exact name", method="get_qs_promoter")
    cost_min = django_filters.NumberFilter(label="Lower bound for min cost", method="get_cost_min")
    cost_max = django_filters.NumberFilter(label="Upper bound for max cost", method="get_cost_max")

    def get_qs_promoter(self, queryset, name, value):
        return queryset.filter(promoters__name=value)

    def get_cost_min(self, queryset, name, value):
        return queryset.filter(priceRanges__type=0, priceRanges__min__gte=value)

    def get_cost_max(self, queryset, name, value):
        return queryset.filter(priceRanges__type=0, priceRanges__max__lte=value)

    class Meta:
        model = models.Event
        fields = ('name', 'start_date',
                  'promoter',
                  "cost_min",
                  "cost_max")


class SaleSerializer(serializers.ModelSerializer):
    """
    Serializer for sub-model of Event
    """
    type = BidirectionalChoiceField(models.Sale.TYPE_CHOICES)

    class Meta:
        model = models.Sale
        fields = (
            "startDateTime",
            "startTBD",
            "endDateTime",
            "type",
            "name",
            "description",
            "url",
        )


class PriceRangeSerializer(serializers.ModelSerializer):
    """
    Serializer for sub-model of Event
    """
    type = BidirectionalChoiceField(models.PriceRange.TYPE_CHOICES)
    currency = BidirectionalChoiceField(models.PriceRange.CURRENCY_CHOICES)

    class Meta:
        model = models.PriceRange
        fields = (
            "type",
            "currency",
            "min",
            "max",
        )


class EventIdLinkSerializer(serializers.ModelSerializer):
    """
    Serializer for representation Promoter related Events.
    See PromoterSerializer.
    """
    id = serializers.PrimaryKeyRelatedField(read_only=True)
    _self = serializers.HyperlinkedIdentityField(view_name="event-detail", read_only=True)

    class Meta:
        model = models.Event
        fields = (
            "id",
            "_self"
        )


class PromoterSerializer(serializers.ModelSerializer):
    """
    Main serializer for promoters endpoint
    """
    events = EventIdLinkSerializer(many=True, source="event", required=False)
    id = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = models.Promoter
        fields = (
            "id",
            "name",
            "description",
            "events",
        )


class EventPromoterSerializer(serializers.ModelSerializer):
    """
    Serializer for representation Promoters as Event sub-model
    """
    id = serializers.PrimaryKeyRelatedField(read_only=True)
    _self = serializers.HyperlinkedIdentityField(view_name="promoter-detail", read_only=True)

    class Meta:
        model = models.Promoter
        fields = (
            "id",
            "name",
            "description",
            "_self",
        )


class EventSerializer(serializers.ModelSerializer):
    """
    Event endpoint serializer
    """
    id = serializers.PrimaryKeyRelatedField(read_only=True)
    _self = serializers.HyperlinkedIdentityField(read_only=True, view_name="event-detail")
    sales = SaleSerializer(many=True, required=True)
    priceRanges = PriceRangeSerializer(many=True, required=True)
    promoters = EventPromoterSerializer(many=True, required=False, read_only=True)
    promoter = serializers.SerializerMethodField(method_name="_get_main_promoter", required=False)

    type = BidirectionalChoiceField(choices=models.Event.TYPE_CHOICES)
    dates_timezone = BidirectionalChoiceField(choices=models.Event.DATES_TIMEZONE_CHOICES)
    locale = BidirectionalChoiceField(choices=models.Event.LOCALE_CHOICES)
    dates_status_code = BidirectionalChoiceField(choices=models.Event.DATES_STATUS_CODE_CHOICES)

    class Meta:
        model = models.Event
        fields = (
            "id",
            "_self",
            "name",
            "type",
            "url",
            "locale",
            "info",
            "pleaseNote",
            "dates_timezone",
            "dates_spanMultipleDays",
            "dates_start_dateTime",
            "dates_start_dateTBD",
            "dates_start_dateTBA",
            "dates_start_timeTBA",
            "dates_start_noSpecificTime",
            "dates_status_code",
            "promoter",
            "promoters",
            "sales",
            "priceRanges",
        )

    def _get_main_promoter(self, obj):
        promoters_filter = obj.promoters.filter(promoting__main=True)
        if promoters_filter.exists():
            target = promoters_filter.first()
            return EventPromoterSerializer(target, context=self.context).data
        return {}
