from django.db.utils import IntegrityError
from rest_framework import status, mixins
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.decorators import permission_classes, api_view, authentication_classes
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from . import models
from .serializers import EventSerializer, PromoterSerializer, EventFilter


class EventViewSet(mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.ListModelMixin,
                   GenericViewSet):
    """
    EventViewSet is restricted by GET, PUT, PATCH requests
    """
    queryset = models.Event.objects.order_by("-id").all()
    serializer_class = EventSerializer
    filter_class = EventFilter

    def update(self, request, *args, **kwargs):
        clean_data, _, _ = split_promoters_and_events(request)
        _, event_serializer = create_or_update_event(clean_data, request=request)
        return Response(event_serializer.data)

    def partial_update(self, request, *args, **kwargs):
        clean_data, _, _ = split_promoters_and_events(request)
        _, event_serializer = create_or_update_event(clean_data, partial=True, request=request)
        return Response(event_serializer.data)


class PromoterViewSet(mixins.RetrieveModelMixin,
                      mixins.ListModelMixin,
                      GenericViewSet):
    """
    PromoterViewSet is restricted by GET request
    """
    queryset = models.Promoter.objects.order_by("id").all()
    serializer_class = PromoterSerializer


@api_view(['GET', 'POST'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes((IsAdminUser,))
def post_raw_event(request):
    """
    Admin tool for startup filling db by raw events
    See tools/events_poster.py module to fill db
    :param request:
    :return: Response with entities added stats
    """
    if request.method == 'GET':
        return Response({'message': 'Access is allowed.'}, status=status.HTTP_200_OK)

    processed_events = []
    processed_promoters = []
    relationships_created = []

    cleaned_data, main_promoter, promoters_data = split_promoters_and_events(request)

    if "priceRanges" not in cleaned_data:
        cleaned_data["priceRanges"] = []
    if "dates_timezone" not in cleaned_data:
        cleaned_data["dates_timezone"] = 'America/New_York'

    create_promoters(promoters_data, processed_promoters)
    event_model, event_serializer = create_or_update_event(cleaned_data, processed_events=processed_events,
                                                           request=request)
    create_promotings(event_model, main_promoter, promoters_data)

    return Response({"events_added": processed_events, "promoters_added": processed_promoters,
                     "relationships_added": relationships_created}, status=status.HTTP_201_CREATED)


def split_promoters_and_events(request):
    """
    Splitting request data to event-content and promoters
    Promoters are read-only, it's restricted to change from events endpoint
    :param request:
    :return: tuple of: cleaned data without promoters,  main promoter object, promoters list
    """
    promoters_data = []
    main_promoter = None
    if "promoters" in request.data:
        promoters_data.extend(request.data["promoters"])
    if "promoter" in request.data:
        main_promoter = request.data["promoter"]
        promoters_data.append(main_promoter)
    cleaned_data = {
        k: request.data[k] for k in filter(lambda key: key not in ("promoter", "promoters"), request.data.keys())
    }
    return cleaned_data, main_promoter, promoters_data


def create_promoters(promoters_data, processed_promoters):
    """
    Creation promoter Models in db
    :param promoters_data: promoter fields
    :param processed_promoters: target to collect promoters processing
    :return: processed promoters stats
    """
    for p in promoters_data:
        promoter_serializer = PromoterSerializer(data=p)
        promoter_serializer.is_valid(raise_exception=True)
        validated_data = promoter_serializer.validated_data
        promoter_id = p["id"]
        found = models.Promoter.objects.get(pk=promoter_id) \
            if models.Promoter.objects.filter(pk=promoter_id).exists() \
            else models.Promoter.objects.create(pk=promoter_id, **validated_data)
        for k, v in validated_data.items():
            setattr(found, k, v)
        found.save()
        processed_promoters.append(promoter_id)
    return processed_promoters


def create_promotings(event_model, main_promoter, promoters_data):
    """
    Create many-to-many relationships in db between Event and Promoter
    :param event_model: model from db
    :param main_promoter: is contained in promoters_data
    :param promoters_data: list of promoter object with "id"
    :return: stats about relationships created stats
    """
    relationships_created = []
    for p in promoters_data:
        promoter_id = p["id"]
        promoter_model = models.Promoter.objects.get(pk=promoter_id)
        promoting = models.Promoting(promoter=promoter_model, event=event_model,
                                     main=main_promoter and main_promoter["id"] == promoter_id)
        try:
            promoting.save()
            relationships_created.append(promoting.id)
        except IntegrityError:
            pass
    return relationships_created


def create_or_update_event(cleaned_data, *, processed_events=None, partial=False, request=None):
    """
    Creates or gets from db Event. Updates Event fields by cleaned_data. Creates subobjects like
    priceRanges and sales, establishes connection between Event, PriceRange, Sale
    :param cleaned_data: event data
    :param processed_events: stats list
    :param partial: True if was called as partial update
    :param request: request for processing in context of request, usually PUT, PATCH. Stores primary key for Event.
    :return: Event model and associated EventSerializer
    """
    if not processed_events:
        processed_events = []

    event_id = cleaned_data["id"] if "id" in cleaned_data else request.parser_context["kwargs"]["pk"]

    event_model = None
    try:
        # ridiculous way to say empty
        event_model = models.Event.objects.get(pk=event_id)
    except models.Event.DoesNotExist:
        pass

    event_serializer = EventSerializer(event_model, data=cleaned_data, partial=partial,
                                       context={'request': request, "pk": event_id})
    event_serializer.is_valid(raise_exception=True)
    event_validated = event_serializer.validated_data

    sales_validated = []
    sales_provided = False
    if "sales" in event_validated:
        sales_validated = event_validated["sales"]
        sales_provided = True
        del event_validated["sales"]

    pr_ranges_validated = []
    pr_ranges_provided = False
    if "priceRanges" in event_validated:
        pr_ranges_validated = event_validated["priceRanges"]
        pr_ranges_provided = True
        del event_validated["priceRanges"]

    event_model and event_serializer.update(event_model, event_validated)
    # escaping TypeError: Direct assignment to the reverse side of a related set is prohibited. Use sales.set() instead.
    event_model = event_serializer.save()

    # if sales were provided it will update sales fields
    if not partial or partial and sales_provided:
        models.Sale.objects.filter(event=event_model).delete()
        for v_data in sales_validated:
            models.Sale.objects.create(event=event_model, **v_data)

    # if PriceRange was provided it will update sales fields
    if not partial or partial and pr_ranges_provided:
        models.PriceRange.objects.filter(event=event_model).delete()
        for v_pr_data in pr_ranges_validated:
            models.PriceRange.objects.create(event=event_model, **v_pr_data)

    processed_events.append(event_id)
    return event_model, event_serializer
