from collections import OrderedDict

from django.conf.urls import url as make_url
from django.urls import path, include
from django.views.generic.base import RedirectView
from rest_framework import routers

from ticketmaster_adapter import endpoints


class EnhancedRootRouter(routers.SimpleRouter):
    """
    Allows append route views to Root API by adding directly
    """
    include_root_view = routers.DefaultRouter.include_root_view
    include_format_suffixes = routers.DefaultRouter.include_format_suffixes
    root_view_name = routers.DefaultRouter.root_view_name
    default_schema_renderers = routers.DefaultRouter.default_schema_renderers
    APIRootView = routers.DefaultRouter.APIRootView
    APISchemaView = routers.DefaultRouter.APISchemaView
    SchemaGenerator = routers.DefaultRouter.SchemaGenerator

    def __init__(self, *args, **kwargs):
        """
        :param args:
        :param kwargs:
            "root_enhancement" is list of tuples ( path, view, basename )
        """
        if 'root_renderers' in kwargs:
            self.root_renderers = kwargs.pop('root_renderers')
        else:
            self.root_renderers = list(routers.api_settings.DEFAULT_RENDERER_CLASSES)

        if "root_enhancement" in kwargs:
            self.root_enhancement = kwargs.pop("root_enhancement")
        else:
            self.root_enhancement = []

        super().__init__(*args, **kwargs)

    def get_api_root_view(self, api_urls=None):
        """
        Return a basic root view.
        """
        api_root_dict = OrderedDict()
        list_name = self.routes[0].name
        for prefix, viewset, basename in self.registry:
            api_root_dict[prefix] = list_name.format(basename=basename)

        for url, view, name in self.root_enhancement:
            api_root_dict[name] = url

        return self.APIRootView.as_view(api_root_dict=api_root_dict)

    def get_urls(self):
        """
        Generate the list of URL patterns, including a default root view
        for the API, and appending `.json` style format suffixes.
        """
        urls = super().get_urls()

        if self.include_root_view:
            view = self.get_api_root_view(api_urls=urls)
            root_url = routers.url(r'^$', view, name=self.root_view_name)
            urls.append(root_url)

        if self.include_format_suffixes:
            urls = routers.format_suffix_patterns(urls)

        for url, view, name in self.root_enhancement:
            enhancement = make_url("^{url}$".format(url=url + "/?"), view, name=name)
            if len(urls) > 1:
                urls.insert(len(urls) - 1, enhancement)
            pass

        return urls


router = EnhancedRootRouter(
    root_enhancement=(
        # admin endpoint for filling db by raw events
        (r'raw_event_poster', endpoints.post_raw_event, "raw_event_poster"), # Api root does not show ???
        # search endpoint, redirects to EventViewSet due to search capabilities
        (r'search', RedirectView.as_view(url="/api/v1/events", query_string=True), "search"),
    )
)
router.register(r'events', endpoints.EventViewSet)
router.register(r'promoters', endpoints.PromoterViewSet)

urlpatterns = [
    # Usage of version schema is skipped
    path('api/v1/', include(router.urls)),
]
