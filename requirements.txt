-i https://pypi.org/simple
asgiref==3.2.7
django-environ==0.4.5
django-filter==2.2.0
django-model-utils==4.0.0
django==3.0.5
djangorestframework==3.11.0
gunicorn==20.0.4
psycopg2-binary==2.8.5
pytz==2019.3
sqlparse==0.3.1
