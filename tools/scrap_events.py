"""
Scraps events from original TM api. Stores every request and merges to single file.
"""
import requests
import json
import os

import environ
launch_env = environ.Env()
launch_env.read_env(
    os.path.join(os.path.dirname(
        os.path.dirname(
            os.path.abspath(__file__)
        )
    ), os.environ.get("ENV_CONFIG")))

TM_KEY = launch_env("TM_API_KEY")
SCRAP_PAGES = 100

if __name__ == "__main__":
    session = requests.session()
    request_template = "https://app.ticketmaster.com/discovery/v2/events.json?apikey={tm_key}&size=50&page={}"

    for p in range(0, SCRAP_PAGES):
        response = session.get(request_template.format(p, tm_key=TM_KEY))
        print("Page {}: status_code {}".format(p, response.status_code))
        if 200 == response.status_code:
            with open("scrapped/{}.json".format(p), "w") as f:
                f.write(response.text)
        else:
            print("Limit exceeded")

    result_events = []
    for p in range(0, SCRAP_PAGES):
        file_name = "scrapped/{}.json".format(p)
        if not os.path.exists(file_name):
            continue
        with open(file_name, "r") as f:
            item = json.load(f)
            result_events.extend(item["_embedded"]["events"])

    with open("scrapped/all_events.json", "w") as f:
        json.dump(result_events, f)