"""
Tool for publishing raw cleaned events to api's db.
Calls endpoint "raw_event_poster" with event data
Uses result of scrap_events.py and models_cleaner.py
Example:
    python events_poster.py events_schema/events_cleaned.json
"""
import json
import os
import sys

import environ
import requests

launch_env = environ.Env()
launch_env.read_env(
    os.path.join(os.path.dirname(
        os.path.dirname(
            os.path.abspath(__file__)
        )
    ), os.environ.get("ENV_CONFIG")))

TARGET_URL = "http://localhost:8000"
# TARGET_URL = "http://157.230.112.196"
LOGIN = launch_env("SU_LOGIN")
PASSWORD = launch_env("SU_PASSWD")

if __name__ == "__main__":
    session = requests.session()
    login_get_resp = session.get("{url}/admin/".format(url=TARGET_URL))
    login_data = {'username': LOGIN, 'password': PASSWORD,
                  'csrfmiddlewaretoken': session.cookies['csrftoken']}
    logged_in_resp = session.post(login_get_resp.request.url, data=login_data)

    with open(sys.argv[1], "r") as f:
        items = json.load(f)

    for i, event in enumerate(items):
        response = session.get("{url}/api/v1/raw_event_poster".format(url=TARGET_URL))

        response = session.post("{url}/api/v1/raw_event_poster".format(url=TARGET_URL),
                                json=event,
                                headers={"X-CSRFToken": session.cookies['csrftoken']}, )
        if response.status_code != 201:
            raise Exception(
                "Error during posting event {}, status code {}\n response: {}".format(event["id"], response.status_code,
                                                                                      response.text))
        print("Posted {}, progress: {} of {}".format(event["id"], i + 1, len(items)))
