"""
Tool for Schema generation from events json. Uses result of models_cleaner.py script.
Uses json file for creation structure with genson and produces raw_models for embedding to Django project.
Marks relationships to parent object. See todos inside models.py.
Example:
    python models_generator.py events_schema/events_cleaned.json events_schema/raw_models.py
"""
import json
import sys
from collections import namedtuple
from datetime import datetime

from genson import SchemaBuilder

model_types = {
    "model": "class {class_name}(models.Model):",
    "many_to_one": "= models.ForeignKey({origin_table}, on_delete=models.CASCADE)",
    "many_to_many": "= models.ManyToManyField({origin_table})",
    "one_to_one": "= models.OneToOneField({origin_table}, on_delete=models.CASCADE)",
    "string": "= models.CharField(max_length=255, {req})",
    "text": "= models.TextField({req})",
    "integer": "= models.IntegerField({req})",
    "boolean": "= models.BooleanField({req})",
    "number": "= models.FloatField({req})",
    "date": "= models.DateField({req})",
    "datetime": "= models.DateTimeField({req})",
    "email": "= models.EmailField(max_length=255, {req})",
}

genson_simple_types = ["string", "boolean", "number", "integer"]
genson_complex_types = ["object", "array"]

Prop = namedtuple("Prop", ["name", "type", "required", "value_instance"])
Relation = namedtuple("Relation", ["name", "type", "to"])


def pascal_case(s):
    return ''.join(x.strip() for x in s.title() if not x.isspace())


def clarify_type(ptype, value_instances):
    if ptype == "string":
        value_instances = [inst.strip().strip("\'").strip("\"") for inst in value_instances]
        try:
            # check all values can be converted to int
            value_instances = [int(inst) for inst in value_instances]
            ptype = "integer"
        except ValueError:
            pass

    if ptype == "string":
        try:
            # check all values can be converted to float
            value_instances = [float(inst) for inst in value_instances]
            ptype = "number"
        except ValueError:
            pass

    if ptype == "string":
        try:
            # check as localDate
            [datetime.strptime(inst, "%Y-%m-%dT%H:%M:%SZ") for inst in value_instances]
            ptype = "datetime"
        except ValueError:
            pass

    if ptype == "string" and max(map(len, value_instances)) > 200:  # could be more
        ptype = "text"

    return ptype, value_instances


class Model:
    models = []
    name_to_model = {}
    need_choises = (
        ("Event", "type"),
        ("Event", "locale"),
        ("Event", "dates_timezone"),
        ("Event", "dates_status_code"),
        ("EventSales", "type"),
        ("EventPriceranges", "type"),
        ("EventPriceranges", "currency"),
    )

    def __init__(self, name):
        self.name = name
        self.props = {}
        self.relations = {}
        Model.models.append(self)
        Model.name_to_model[self.name] = self

    def add_prop(self, name, ptype, required, value_instances):
        ptype, value_instances = clarify_type(ptype, value_instances)
        self.props[name] = Prop(name, ptype, required, set(value_instances))

    def add_rel(self, name, rtype, to):
        self.relations[name] = Relation(name, rtype, to)

    def generate_choises(self, prop):
        for nc in Model.need_choises:
            if nc[0] == self.name and prop.name == nc[1]:
                choises = "\t{upper_name}_CHOICES = {content}".format(upper_name=prop.name.upper(),
                                                                    content=tuple(enumerate(prop.value_instance)))
                return choises

    def __str__(self):
        result = [model_types["model"].format(class_name=self.name)]
        for prop in self.props.values():
            type_s = model_types.get(prop.type, "UNKNOWN")
            if type_s == "UNKNOWN":
                print("not found {}".format(prop.type))
            type_s = type_s.format(req="" if prop.required else "null=True")

            choises = self.generate_choises(prop)
            if choises:
                result.append(choises)


            result.append(
                ("\t# Instance: {ex}\n\t#set_size:{ss} item_len:{il}\n" +
                 "\t{name} {type}\n\n").format(name=prop.name,
                                           type=type_s,
                                           ex=repr(list(prop.value_instance)[:4]),
                                           ss=len(prop.value_instance),
                                           il=max(map(len, prop.value_instance)) if prop.type == "string" else 0
                                           )
            )
        for rel in self.relations.values():
            result.append("\t# todo: {to} contains as {type}".format(name=rel.name, type=rel.type, to=rel.to))
        return "\n".join(result)

    @classmethod
    def create(cls, path):
        name = pascal_case(" ".join(path))
        is_present = Model.name_to_model.get(name, None)
        return is_present if is_present else Model(name)

    @classmethod
    def dump(cls, file):
        with open(file, "w") as f:
            f.write("from django.db import models\n\n")
            for model in cls.models:
                f.write(str(model))
                f.write("\n\n")



def search_value_instance(instance, path):
    result = []
    found = False
    if not len(path):
        return [instance], True
    if type(instance) == list:
        for element in instance:
            res, fnd = search_value_instance(element, path)
            found = found or fnd
            result.extend(res)
    else:
        if path[0] in instance:
            res, fnd = search_value_instance(instance[path[0]], path[1:])
            found = found or fnd
            result.extend(res)
    return result, found


def create_models(event_schema, path, instances):
    model = Model.create(path)

    if "items" in event_schema and event_schema["type"] == "array":
        event_schema = event_schema["items"]

    for name, info in event_schema["properties"].items():
        prop_type = info["type"]
        if prop_type not in genson_complex_types and prop_type not in genson_simple_types:
            raise Exception("Unknown type: {}".format(prop_type))

        if prop_type in genson_simple_types:
            instance, found = search_value_instance(instances, [*path[1:], name])  # skip artificial Events node
            if not found:
                raise Exception("Not found instance of {}".format([*path[1:], name]))

            model.add_prop(name, prop_type, name in event_schema["required"],
                           instance)

        if prop_type in genson_complex_types:
            sub_model = Model.create([*path, name])
            sub_model.add_rel(name, prop_type, model.name)
            create_models(info, [*path, name], instances)


def load_events(input):
    with open(input, "r") as f:
        datastore = json.load(f)
    return datastore


def generate_schema(datastore):
    builder = SchemaBuilder()
    for event in datastore:
        builder.add_object(event)
    return builder.to_schema()


if __name__ == "__main__":
    args = sys.argv[1:]
    if len(args) != 2:
        print("Usage:\n python models_generator.py events_schema/events_cleaned.json events_schema/raw_models.py")
        sys.exit(1)

    datastore = load_events(args[0])
    schema = generate_schema(datastore)
    create_models(schema, ["event"], datastore)
    Model.dump(args[1])
