#!/usr/bin/env sh
# manual adding user to psql
psql <<EOF
CREATE DATABASE tm_db;
CREATE USER tm_user_example WITH PASSWORD 'tm_password_example';
ALTER ROLE tm_user_example SET client_encoding TO 'utf8';
ALTER ROLE tm_user_example SET default_transaction_isolation TO 'read committed';
ALTER ROLE tm_user_example SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE tm_db TO tm_user_example;
EOF