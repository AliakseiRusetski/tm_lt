"""
Tool for restructuring and cleaning original events from TM.
Reads from json file and produces json file.
Uses result of scrap_events.py
Example:
    python models_cleaner.py scrapped/all_events.json events_schema/events_cleaned.json
"""
import json
import sys


def remove(o, del_fields):
    if type(del_fields) == tuple:
        for del_it in del_fields:
            if del_it in o:
                del o[del_it]
    else:
        for k, v in del_fields.items():
            remove(o[k], v)
    pass


def flatten_dicts(o, fields=None, delimeter="_"):
    if not isinstance(o, dict):
        return None

    if fields:
        keys = fields
    else:
        keys = tuple(o.keys())

    for field in keys:
        flattened = flatten_dicts(o[field])
        if flattened:
            del o[field]
            for k, v in flattened.items():
                o[delimeter.join([field, k])] = v
    return o


def inverse_sales(result):
    sales = []
    for sale_type, sale_value in result["sales"].items():
        if isinstance(sale_value, list):
            for sale_v in sale_value:
                sale_v["type"] = sale_type
                sales.append(sale_v)
        else:
            sale_value["type"] = sale_type
            sales.append(sale_value)
    result["sales"] = sales
    pass


def downgrade_fields(event, rules):
    if isinstance(event, list):
        for el in event:
            downgrade_fields(el, rules)
    elif isinstance(event, dict):
        keys = tuple(event.keys())
        for k in keys:
            downgrade_fields(event[k], rules)
            if k in rules:
                event[rules[k]] = event[k]
                del event[k]


event_keys = []
promoter_keys = []

def reorder_primary_keys(target, keys):
    tk = target["id"]
    if tk not in keys:
        keys.append(tk)
    target["id"] = keys.index(tk)
    pass


def cleanup(raw_event):
    result = {}
    keep_fields = (
        "name", "type", "id", "url", "locale", "info", "pleaseNote",
        "sales", "dates", "priceRanges", "promoter", "promoters",
    )
    for keep_field in keep_fields:
        if keep_field in raw_event:
            result[keep_field] = raw_event[keep_field]

    remove_fields = ({
        "dates": {
            "start": ("localDate", "localTime")
        }
    })
    remove(result, remove_fields)

    flatten_dicts(result, ("dates",))
    inverse_sales(result)
    # downgrade_fields(result, {"id": "t_id"})
    reorder_primary_keys(result, event_keys)

    promoters = []
    if "promoter" in event:
        promoters.append(event["promoter"])
    if "promoters" in event:
        promoters.extend(event["promoters"])
    for p in promoters:
        reorder_primary_keys(p, promoter_keys)
    return result


if __name__ == "__main__":
    args = sys.argv[1:]
    if len(args) != 2:
        print("Usage:\n python models_cleaner.py scrapped/all_events.json events_schema/events_cleaned.json")
        sys.exit(1)

    raw_event = None
    with open(sys.argv[1], "r") as f:
        raw_event = json.load(f)

    result = []
    for event in raw_event:
        result.append(cleanup(event))

    with open(sys.argv[2], "w") as f:
        json.dump(result, f)
