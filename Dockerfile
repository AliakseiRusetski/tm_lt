FROM python:3.7-alpine

ENV DEBUG="False"
ENV SECRET_KEY="a0dj!9n63-48fcvg1wf#j03%d=c@o8e0&8%-!$2#%1a!5r72#i"
ENV DATABASE_URL="sqlite:///db.sqlite3"
ENV LANG="en_US.UTF-8"
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

LABEL author="rusetskyalex"
LABEL description="Django tm adapter web app"

WORKDIR /home/tm/
COPY requirements.txt requirements.txt
COPY project project
COPY ticketmaster_adapter ticketmaster_adapter
COPY static static
COPY manage.py manage.py
COPY .env .env

# install support files for psql
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
# install project requirements
# skipped direct wheel/egg installation, skipping cache dir for pip, linting
RUN chmod +x manage.py && chmod +x ticketmaster_adapter/on_db_ready_start.sh && pip install -r requirements.txt
# fix layer with passign tests
RUN ./manage.py test

EXPOSE 8000

# for local launch
# CMD ["./manage.py", "migrate"]
# CMD ["./manage.py", "runserver", "0.0.0.0:8000"]

# lock entrypoint on db ready
ENTRYPOINT ["./ticketmaster_adapter/on_db_ready_start.sh"]
