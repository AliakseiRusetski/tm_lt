### Task
In terms of our task, we would like you to:

1. Create a new web application using Django

2. Scrape the Ticketmaster Discovery API for events
https://developer.ticketmaster.com/products-and-docs/apis/discovery-api/v2/
Don’t scrape everything. 50 pages should be enough because of API keys are limited by 5k requests per day.

3. Store and/or structure the events that you scrape into your Django application. Event should have at least: event name, promoter name if it exists, description, multiple prices, url, start date, finish date)

4. Inside your Django app, create a searchable api endpoint (returns json) allow the api request to search for event name, event start date, promoter name, ticket cost (min and max for a standard price).

5. Inside your application, create 1 API endpoint that accepts json that allows a user to update the locally stored event record, create some arbitrary validations.

6. Send us the code and the URL to your hosted application.

*. Write Tests you find necessary

### Description
- API root contains endpoints
- Scrapped by tools/
- Event structured to models: Event, Sale, PriceRange, Promoter, Promoting (many-to-many relationship handler)
- Search endpoint forwards to Event which supports search queries
- Event endpoint supports PUT, PATCH methods for updating content
- Admin endpoint has registered superuser
- A few smoke tests are present

### Endpoints
- /api/v1/ - APIRoot
- /api/v1/events - List of events 
- /api/v1/promoters - List of promoters 
- /api/v1/raw_event_poster - Admin endpoint for filling starting content 
- /api/v1/search - Search endpoint, forwards to events with search parameters
- /admin - DjangoAdmin 

### Steps
- Created django app;
- Added rest_framework to settings.py 
- Added LOGGING to settings.py 
- Added module for scrapping events tools/scrap_events.py
- Added module for cleanup tools/models_cleaner.py
- Added module for generation Django Models tools/models_generator.py
- Fixed few times raw_models.py and moved to ticketmaster_adapter/models.py
- Fixed relationships for models
- Added endpoint for filling db by scrapped events
- Added ViewSets for Promoters and Events
- Limited REST operations
- Created Serializers for events ViewSet
- Created super_user login / password
- Created .env file for starting
- Created distinct endpoint post "raw_event_poster", for filling db by content
- Limited "raw_event_poster" to admin users
- Added models to admin site
- Fixing endpoints
- Reordered models and relationships 
- Created embedded serializers for sub-models
- Generated requirements.txt
- Created smoke tests
- Created dockerfile for application
- Tested psql connection and tested
- Created nginx subdir and config
- Building by docker-compose locally, waiting for db script
- Pushed to hub.docker.com
- Pushed to bitbucket